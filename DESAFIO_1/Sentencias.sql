CREATE DATABASE ColegioFAlso
CREATE TABLE Profesor(
    ID_Profesor int primary key,
    Nombre_Profesor varchar(50),
    Apellido_Profesor varchar(50)
);
CREATE TABLE Materia(
    Sigla_Materia varchar(10) primary key,
    Nombre_Materia varchar(20)
);
CREATE TABLE Alumno(
    ID_Alumno int primary key,
    Nombre_Alumno varchar(50),
    Apellido_Alumno varchar(50),
    Edad int,
    Sexo varchar(1)
);
CREATE TABLE Imparte(
    Sigla_Materia varchar(10),
    ID_Profesor int,
    FOREIGN KEY (Sigla_Materia) REFERENCES Materia(Sigla_Materia),
    FOREIGN KEY (ID_Profesor) REFERENCES Profesor(ID_Profesor)
)
CREATE TABLE Cursa(
    Sigla_Materia varchar (10),
    ID_Alumno int,
    Nota_Materia int,
    FOREIGN KEY (Sigla_Materia) REFERENCES Materia(Sigla_Materia),
    FOREIGN KEY (ID_Alumno) REFERENCES Alumno(ID_Alumno)
);
